package edu.eam.ingesoft.students.externalapi.model.responses

data class ProgramResponse(
    val id: Long,
    val name: String
)
