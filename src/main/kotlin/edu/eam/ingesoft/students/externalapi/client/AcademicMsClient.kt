package edu.eam.ingesoft.students.externalapi.client

import edu.eam.ingesoft.students.externalapi.model.responses.AcademicProgramResponse
import edu.eam.ingesoft.students.externalapi.model.responses.FacultyResponse
import edu.eam.ingesoft.students.externalapi.model.responses.ProgramResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@FeignClient(name = "AcademicMsClient", url = "\${externalapi.academic-ms}/api/academic-ms")
interface AcademicMsClient {
    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/programs/{idProgram}"))
    fun getProgram(@PathVariable("idProgram") idProgram: Long): ProgramResponse

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/faculties/{id}"))
    fun getFacultyById(@PathVariable idFaculty: Long): FacultyResponse

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/programs/{idFaculty}/programs"))
    fun getAllProgramsFromFaculty(@PathVariable idFaculty: Long): List<AcademicProgramResponse>

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("programs/{idProgram}"))
    fun getAcademicProgram(@PathVariable idProgram: String): AcademicProgramResponse
}
