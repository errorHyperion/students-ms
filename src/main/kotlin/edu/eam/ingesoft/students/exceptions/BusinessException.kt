package edu.eam.ingesoft.students.exceptions

import org.springframework.http.HttpStatus

class BusinessException(message: String?, val status: HttpStatus = HttpStatus.PRECONDITION_FAILED) :
    RuntimeException(message)
