package edu.eam.ingesoft.students.config

object Permissions {
    const val DEACTIVATE_STUDENT = "deactivateStudent"
    const val ACTIVATE_STUDENT = "activateStudent"
    const val LIST_STUDENT_BY_ACADEMIC_PROGRAM = "listStudentByAcademicProgram"
    const val FIND_STUDENT = "findStudent"
    const val EDIT_STUDENT = "editStudent"
    const val CREATE_STUDENTS = "createStudents"
}
