package edu.eam.ingesoft.students.config

object Routes {
    const val PATH = "/path"
    const val STUDENTS_PATH = "/students"
    const val FACULTY_PATH = "/faculties"
    const val ACADEMIC_PROGRAM_PATH = "/programs"
    const val EDIT_STUDENT_PATH = "/{code}"
    const val FIND_STUDENT_PATH = "/{code}"
    const val ACTIVATE_STUDENT_PATH = "/{code}/activate"
    const val DEACTIVATE_STUDENT_PATH = "/{code}/deactivate"
    const val LIST_STUDENTS_BY_PROGRAM_PATH = "/{idProgram}/students"
    const val FIND_STUDENTS_FACULTY_PATH = "/{idFaculty}/students"
}
