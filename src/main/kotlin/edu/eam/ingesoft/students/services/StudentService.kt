package edu.eam.ingesoft.students.services

import edu.eam.ingesoft.students.exceptions.BusinessException
import edu.eam.ingesoft.students.externalapi.services.AcademicMsService
import edu.eam.ingesoft.students.externalapi.services.AcademicProgramService
import edu.eam.ingesoft.students.model.entities.Student
import edu.eam.ingesoft.students.repositories.PersonRepository
import edu.eam.ingesoft.students.repositories.StudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class StudentService {

    @Autowired
    lateinit var studentRepository: StudentRepository

    @Autowired
    lateinit var personRepository: PersonRepository

    @Autowired
    lateinit var academicMsService: AcademicMsService

    @Autowired
    lateinit var academicProgramService: AcademicProgramService

    fun create(student: Student) {

        academicProgramService.getAcademicProgramById(student.programId)

        val personId = student.person.identificationNumber

        val personToFind = personRepository.findById(personId)

        if (personToFind.isEmpty) {

            val personByEmail = personRepository.findByEmail(student.person.email)

            if (personByEmail.isNotEmpty()) throw BusinessException(
                "Email is already in use.",
                HttpStatus.PRECONDITION_FAILED
            )

            val personByPhoneNumber = personRepository.findByPhoneNumber(student.person.phoneNumber)

            if (personByPhoneNumber.isNotEmpty()) throw BusinessException(
                "Phone number is already in use.",
                HttpStatus.PRECONDITION_FAILED
            )

            personRepository.save(student.person)

            val studentCode = student.code

            val studentToFind = studentRepository.findById(studentCode)

            if (studentToFind.isPresent) throw BusinessException(
                "Code is already in use.",
                HttpStatus.PRECONDITION_FAILED
            )

            studentRepository.save(student)
        } else {
            val studentCode = student.code

            val studentToFind = studentRepository.findById(studentCode)

            if (studentToFind.isPresent) throw BusinessException(
                "Code is already in use.",
                HttpStatus.PRECONDITION_FAILED
            )

            studentRepository.save(student)
        }
    }

    fun find(code: String): Student =
        studentRepository.findById(code).orElseThrow { EntityNotFoundException("Student not found.") }

    fun edit(student: Student, code: String) {

        val studentToFind = studentRepository.findById(code)

        if (studentToFind.isEmpty) throw BusinessException("Student doesn't exist.", HttpStatus.NOT_FOUND)

        val personByEmail = personRepository.findByEmail(student.person.email)

        if (personByEmail.isNotEmpty() && personByEmail[0].identificationNumber != studentToFind.get().person.identificationNumber) throw BusinessException(
            "Email is already in use.",
            HttpStatus.PRECONDITION_FAILED
        )

        val personByPhoneNumber = personRepository.findByPhoneNumber(student.person.phoneNumber)

        if (personByPhoneNumber.isNotEmpty() && personByPhoneNumber[0].identificationNumber != studentToFind.get().person.identificationNumber) throw BusinessException(
            "Phone number is already in use.",
            HttpStatus.PRECONDITION_FAILED
        )

        val studentId = studentRepository.findById(code)

        if (student.code != code && studentRepository.existsById(student.code)) throw BusinessException(
            "Code is already in use.",
            HttpStatus.PRECONDITION_FAILED
        )

        academicMsService.findProgram(student.programId.toLong())

        if (student.person.identificationNumber != studentId.get().person.identificationNumber && personRepository.existsById(student.person.identificationNumber)
        ) throw BusinessException("Identification number is already in use.", HttpStatus.PRECONDITION_FAILED)

        personRepository.save(student.person)

        studentRepository.save(student)
    }

    fun activate(code: String) {
        val studentToFind = studentRepository.findById(code).orElse(null) ?: throw BusinessException(
            "Student doesn't exist.",
            HttpStatus.NOT_FOUND
        )

        if (studentToFind.enabled == true) throw BusinessException(
            "Student already activated.",
            HttpStatus.PRECONDITION_FAILED
        )

        studentToFind.enabled = true

        studentRepository.save(studentToFind)
    }

    fun deactivate(code: String) {
        val studentToFind = studentRepository.findById(code).orElse(null)
            ?: throw BusinessException("Student doesn't exist.", HttpStatus.NOT_FOUND)

        if (studentToFind.enabled == false) throw BusinessException(
            "Student already deactivate.",
            HttpStatus.PRECONDITION_FAILED
        )

        studentToFind.enabled = false

        studentRepository.save(studentToFind)
    }

    fun findStudentsByProgramId(pageable: Pageable, programId: String): Page<Student> {
        academicMsService.findProgram(programId.toLong())

        return studentRepository.findByProgramId(pageable, programId)
    }

    fun findStudentsByFacultyId(facultyId: Long, pageable: Pageable): Page<Student> {

        val programsToFind = academicMsService.getProgramByFacultyId(facultyId).map { it.id.toString() }

        return studentRepository.findStudentsByAcademicProgramId(programsToFind, pageable)
    }
}
