package edu.eam.ingesoft.students.controllers

import edu.eam.ingesoft.students.config.Groups
import edu.eam.ingesoft.students.config.Permissions
import edu.eam.ingesoft.students.config.Routes
import edu.eam.ingesoft.students.model.entities.Student
import edu.eam.ingesoft.students.security.Secured
import edu.eam.ingesoft.students.services.StudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.STUDENTS_PATH)
class StudentController {

    @Autowired
    lateinit var studentService: StudentService

    @Secured(permissions = [Permissions.CREATE_STUDENTS], groups = [Groups.REGISTRATION_AND_CONTROL, Groups.SYSTEM_ADMINISTRATOR])
    @PostMapping
    fun create(@RequestBody student: Student) = studentService.create(student)

    @Secured(permissions = [Permissions.EDIT_STUDENT], groups = [Groups.REGISTRATION_AND_CONTROL])
    @PutMapping(Routes.EDIT_STUDENT_PATH)
    fun edit(@RequestBody student: Student, @PathVariable("code") code: String) = studentService.edit(student, code)

    @Secured(
        permissions = [Permissions.FIND_STUDENT],
        groups = [Groups.CLASS_TEACHER, Groups.PROGRAM_DIRECTOR, Groups.REGISTRATION_AND_CONTROL, Groups.SYSTEM_ADMINISTRATOR]
    )
    @GetMapping(Routes.FIND_STUDENT_PATH)
    fun find(@PathVariable("code") code: String) = studentService.find(code)

    @Secured(permissions = [Permissions.ACTIVATE_STUDENT], groups = [Groups.SYSTEM_ADMINISTRATOR, Groups.REGISTRATION_AND_CONTROL])
    @PatchMapping(Routes.ACTIVATE_STUDENT_PATH)
    fun activate(@PathVariable("code") code: String) = studentService.activate(code)

    @Secured(permissions = [Permissions.DEACTIVATE_STUDENT], groups = [Groups.REGISTRATION_AND_CONTROL, Groups.SYSTEM_ADMINISTRATOR])
    @PatchMapping(Routes.DEACTIVATE_STUDENT_PATH)
    fun deactivate(@PathVariable("code") code: String) = studentService.deactivate(code)
}
