package edu.eam.ingesoft.students.repositories

import edu.eam.ingesoft.students.model.entities.Person
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface PersonRepository : CrudRepository<Person, String> {

    fun findByPhoneNumber(phoneNumber: String): List<Person>

    fun findByEmail(mail: String): List<Person>
}
