package edu.eam.ingesoft.students.security.authclient.model

data class Token(
    val token: String,
)
