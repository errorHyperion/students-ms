package edu.eam.ingesoft.students.security

import java.lang.RuntimeException

class SecurityException(message: String?) : RuntimeException(message)
