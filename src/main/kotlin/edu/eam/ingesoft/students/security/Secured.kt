package edu.eam.ingesoft.students.security

@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Secured(
    val permissions: Array<String> = arrayOf(),
    val groups: Array<String> = arrayOf()
)
