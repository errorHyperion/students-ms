package edu.eam.ingesoft.students.model.entities

import edu.eam.ingesoft.students.model.enums.DocumentTypeEnum
import edu.eam.ingesoft.students.model.enums.GenderEnum
import org.springframework.format.annotation.NumberFormat
import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Temporal
import javax.persistence.TemporalType
import javax.validation.constraints.Email

@Entity
@Table(name = "person")
data class Person(
    @Id
    @Column(name = "identification_number")
    val identificationNumber: String,

    val name: String,

    @Column(name = "last_name")
    val lastName: String,

    @Enumerated(EnumType.STRING)
    val gender: GenderEnum,

    @Email
    val email: String,

    @NumberFormat
    @Column(name = "phone_number")
    val phoneNumber: String,

    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date")
    val birthDate: Date,

    @Enumerated(EnumType.STRING)
    val documentType: DocumentTypeEnum,

    val enabled: Boolean? = true
)
