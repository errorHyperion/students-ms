CREATE TABLE public.person (
	identification_number varchar(255) NOT NULL,
	birth_date date NULL,
	document_type varchar(255) NULL,
	email varchar(255) NULL,
	enabled bool NULL,
	gender varchar(255) NULL,
	last_name varchar(255) NULL,
	"name" varchar(255) NULL,
	phone_number varchar(255) NULL,
	CONSTRAINT person_pkey PRIMARY KEY (identification_number)
);

CREATE TABLE public.student (
	code varchar(255) NOT NULL,
	enabled bool NULL,
	program_id varchar(255) NULL,
	person_id varchar(255) NULL,
	CONSTRAINT student_pkey PRIMARY KEY (code),
	CONSTRAINT fknwsufvlvlnsxqv60ltj06bbfx FOREIGN KEY (person_id) REFERENCES person(identification_number)
);